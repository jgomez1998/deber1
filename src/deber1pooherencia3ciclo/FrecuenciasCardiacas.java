/*
 * (Calculadora de la frecuencia cardiaca esperada) Mientras se ejercita, puede usar
un monitor de frecuencia cardiaca para ver que su corazón permanezca dentro de un rango
seguro sugerido por sus entrenadores y doctores. De acuerdo con la Asociación Estadounidense 
del Corazón (AHA) (vwwv. ameri canheart.org/), la fórmula para calcular su frecuencia cardiaca
máxima en pulsos por minuto es de 220 menos su edad en años. Su frecuencia cardiaca esperada 
es un rango que está entre el 50 y el 85% de su frecuencia cardiaca máxima. [Nota: estas fórmulas
son estimaciones proporcionadas por la AHA. Las frecuencias cardiacas máxima y esperada pueden 
variar de acuerdo con la salud, condición física y sexo del individuo. Siempre debe consultar un 
médico o a un profesional de la salud antes de empezar o modificar un programa de ejercicios.] 
Cree una clase llamada FrecuenciasCardiacas. Los atributos de la clase deben incluir el primer 
nombre de la persona, su apellido y fecha de nacimiento (la cual debe consistir de atributos 
separados para el mes, día y año de nacimiento). Su clase debe tener un constructor que reciba
estos datos como parámetros. Para cada atributo debe proveer métodos establecery obtener. La 
clase también debe incluir un método que calcule y devuelva la edad de la persona (en años), 
uno que calcule y devuelva la frecuencia cardiaca máxima de esa persona, y otro que calcule 
y devuelva la Secuencia cardiaca esperada de la persona. Escriba una aplicación de Java que 
pida la información de la persona, cree una instancia de un objeto de la clase FrecuenciasCardiacase 
imprima la información a partir de ese objeto (incluya el primer nombre de la persona, su apellido 
y fecha de nacimiento), y que después calcule e imprima la edad de la persona en (años), Secuencia 
cardiaca máxima y rango de frecuencia cardiaca esperada.
 */
package deber1pooherencia3ciclo;
import java.util.Scanner;
public class FrecuenciasCardiacas {
    String nombre, apellido;
    int dia, mes, anio;
    int anioAct = 2018, edad;
    int frecuenciaMax;
    double frecuenciaMaxEsp, frecuenciaMinEsp;
    public FrecuenciasCardiacas(String nombre, String apellido, int dia, int mes, int anio) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public int getDia() {
        return dia;
    }
    public void setDia(int dia) {
        this.dia = dia;
    }
    public int getMes() {
        return mes;
    }
    public void setMes(int mes) {
        this.mes = mes;
    }
    public int getAnio() {
        return anio;
    }
    public void setAnio(int anio) {
        this.anio = anio;
    }
    public int getAnioAct() {
        return anioAct;
    }
    public void setAnioAct(int anioAct) {
        this.anioAct = anioAct;
    }
    public void edadPersona(){
        edad = anioAct - this.getAnio();
        //System.out.println(edad);
    }
    public void frecuenciaCardiacaMax(){
        frecuenciaMax = 220 - edad;
        //System.out.println(frecuenciaMax);
    }
    public void frecuenciaEsperada(){
        frecuenciaMinEsp= (frecuenciaMax*50)/100;
        frecuenciaMaxEsp= (frecuenciaMax*85)/100;
    }
    @Override
   public String toString(){
       edadPersona();
       frecuenciaCardiacaMax();
       frecuenciaEsperada();
       return nombre+" "+apellido+" nacido el "+dia+"/"+mes+"/"+anio+"\nEdad Actual de "+edad+" años"+
               "\nCon Frecuencia Máxima de "+frecuenciaMax+"\nRango de Frecuencia esperado entre "+
               frecuenciaMinEsp+" y "+frecuenciaMaxEsp;
   } 
}
class pruebaFrecuencias{
    public static void main(String[] args) {
        Scanner teclado = new Scanner (System.in);
        String nombre, apellido;
        int dia, mes, anio;
        System.out.print("Ingrese su nombre: "); nombre = teclado.next();
        System.out.print("Ingrese su apellido: "); apellido = teclado.next();
        System.out.print("Ingrese su dia de nacimiento: "); dia = teclado.nextInt();
        System.out.print("Ingrese su mes de nacimiento: "); mes = teclado.nextInt();
        System.out.print("Ingrese su año de nacimiento: ");anio = teclado.nextInt();
        FrecuenciasCardiacas persona1 = new FrecuenciasCardiacas(nombre, apellido, dia, mes, anio); 
        System.out.println(persona1);
    }  
}